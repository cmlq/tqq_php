<?php
//授权回调页面，即配置文件中的$callback_url
session_start(); //此示例中要使用session
require_once('config.php');
require_once('tqq.php');

if(isset($_GET['code']) && $_GET['code']!=''){
	$tqq=new tqqPHP($tqq_k, $tqq_s);
	$result=$tqq->access_token($callback_url, $_GET['code']);
}
if(isset($result['access_token']) && $result['access_token']!='' && isset($_GET['openid']) && $_GET['openid']!=''){
	echo '授权完成，请记录<br/>access token：<input size="50" value="',$result['access_token'],'"><br/>openid：<input size="50" value="',$_GET['openid'],'"><br/>refresh token：<input size="50" value="',$result['refresh_token'],'">';

	//保存登录信息，此示例中使用session保存
	$_SESSION['tqq_t']=$result['access_token']; //access token
	$_SESSION['tqq_id']=$_GET['openid']; //openid
	$_SESSION['tqq_r']=$result['refresh_token']; //refresh token
}else{
	echo '授权失败';
}
echo '<br/><a href="demo.php">返回</a>';
