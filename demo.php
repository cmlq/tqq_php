<?php
session_start(); //此示例中要使用session
require_once('config.php');
require_once('tqq.php');

function getimgp($u){
	//图片处理
	$c=@file_get_contents($u);
	$name=md5($u).'.jpg';
	$mime='image/unknown';
	return array($mime, $name, $c);
}

$tqq_t=isset($_SESSION['tqq_t'])?$_SESSION['tqq_t']:'';
$tqq_id=isset($_SESSION['tqq_id'])?$_SESSION['tqq_id']:'';

//检查是否已登录
if($tqq_t!='' && $tqq_id!=''){
	$tqq=new tqqPHP($tqq_k, $tqq_s, $tqq_t, $tqq_id);

	//获取登录用户信息
	$result=$tqq->me();
	var_dump($result);

	/**
	//access token到期后使用refresh token刷新access token
	$result=$tqq->access_token_refresh($_SESSION['tqq_r']);
	var_dump($result);
	**/

	/**
	//发布微博
	$img='http://www.baidu.com/img/baidu_sylogo1.gif';
	$img_a=getimgp($img);
	if($img_a[2]!=''){
		$p_img=$img_a;
	}else{
		$p_img='';
	}
	$result=$tqq->postOne('微博内容', $p_img);
	var_dump($result);
	**/

	/**
	//微博列表
	$result=$tqq->getMyTweet();
	var_dump($result);
	**/

	/**
	//其他功能请根据官方文档自行添加
	//示例：获取登录用户信息
	$result=$tqq->api('user/info', array(), 'GET');
	var_dump($result);
	**/

}else{
	//生成登录链接
	$tqq=new tqqPHP($tqq_k, $tqq_s);
	$login_url=$tqq->login_url($callback_url);
	echo '<a href="',$login_url,'">点击进入授权页面</a>';
}
